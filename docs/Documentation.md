## Project Structure

```bash
./cprep-test/
├── babel.config.js
├── docs
│   ├── Documentation.md                   # you are here
│   ├── GetStarted.md                      # Overview and installation
│   
├── package.json
├── package-lock.json
├── public
│   ├── favicon-32x32.png
│   ├── favicon.ico
│   └── index.html                         # main html file
├── README.md
└── src
    ├── App.vue                            # root vue components
    ├── assets
    │   ├── icons
    │   │   └── icons.js                   # import required core ui icons from here
    │   ├── images
    │   │   └── csbgd.png                  # login page background
    │   └── logo.png
    ├── auth
    │   ├── AutoLogin.vue
    │   ├── Login.vue
    │   └── SignUp.vue                     # not used
    ├── components
    │   ├── Container.vue                  
    │   ├── Footer.vue                     # not used
    │   └── Header.vue                     # App header,handles start/stop timer
    ├── main.js                            # <3 of the application
    ├── mixins
    │   ├── HttpCommon.js                  # handles ajax calls, axios used.
    │   ├── localDb.js                     # helper functions used in different views and components.
    │   └── proctoring.js                  # handles video and image proctoring.
    ├── router
    │   └── index.js
    ├── store
    │   └── index.js                       # local store to track timer and exam status
    └── views
        ├── exam
        │   ├── ExamDetails.vue            # instruction for taking exam and handles proctoring settings
        │   ├── ExamEnd.vue                # page with logout button
        │   ├── ExamFeedback.vue           # feedback form and final submit
        │   └── paper
        │       ├── components
        │       │   ├── Legend.vue                  # display count of each status
        │       │   ├── QuestionNumberPanel.vue     # handles question navigation
        │       │   └── QuestionTab.vue             # display question with options
        │       └── ExamPaper.vue                   # handles exam flow and proctoring
        └── Result.vue

```


/***********************************************************************************************************/


### View: ExamDetails.vue

> NOTE: **function() are present in mixins

props: none

mixins: ajaxCallMixin, localDb

components: none

lifecycle hooks:
    beforeMount()
        get 'examData' from localStorage
        methods: **get()

navigation gaurds:
    beforeRouteEnter()
        push to 'exam/paper' if already started the exam

computed:
    getInstructions()
        returns exam instructions in html format

methods:
    getExamQuestions()
        purpose: get questions for each module
        parameters: none
        methods: **ajaxCall()
        API call: yes
            url: '/test/test/getModuleQuestions'
            callback: startExam()

    startExam() [async]
        purpose: store the module question in session storage and move to exam page
        parameters: none
        methods: applyExamSettings()

    applyExamSettings() [async]
        purpose: apply video, image proctoring and geo location according to exam settings
        parameters: none
        methods: proctoring(), geoLocation()
        return true if required settings are applied else false

    proctoring()
        purpose: get access for video and audio according to given flags
        parameters: vidFlag, imgFlag. possible values - '0', '1', '2'
        methods: addVideoTag()
        return resolved promise if user give access for required flag else rejects
    
    addVideoTag()
        purpose: create a hidden video element and bind the input stream to it.
        parameters: stream
        methods: none
        return none

    geoLocation()
        purpose: get user location
        parameters: geoLocFlag. possible values - '0', '1', '2'
        methods: getCurrentLoction(), sendUserLocation()
        return resolved promise if user give access for required flag else rejects
    
    getCurrentLoction()
        purpose: wrapper to make default 'navigator.geoLocation.getCurrentLoction' return a promise
        parameters: none
        methods: none
        return resolved if user allows else rejects

    sendUserLocation()
        purpose: send longitude and latitude to server
        parameters: position 
        methods: **ajaxCall()
        API call: yes
            url: 'test/proctoring/uploadGeoLocation'
            callback: none
    


/***********************************************************************************************************/

### View --> ExamPaper.vue

>>NOTE: **function() are present in mixins

>>NOTE: To view how proctoring is handled visit proctoring.js in mixins folder

props: none

mixins: ajaxCallMixin, localDb, proctoring

components: QuestionNumberPanel.vue, QuestionTab.vue

lifecycle hooks:
    beforeMount()
        purpose: 1) add event listner 'beforeunload' to save exam state on reload, 
                 2) add event listner 'blur' to get offFocusCount
        methods: saveState()

    beforeDestroy()
        purpose: remove all manually added event listners

    mounted()
        purpose: to start timer and load data from session storage
        methods: getExamPaperData()

navigation gaurds:
    beforeRouteEnter()
        push to 'exam/feedback' if exam has been submitted
        push to 'exam/details' if exam not started yet

    beforeRouteLeave()
        block navigation to 'exam/details', to prevent back button

watcher:
    tabIndex: changes module on tab switch as tab component updates this value

    isExamInProgress: submit exam if it changes to false

computed:
    mapGetters:
        timer: state of timer on/off
        isExamInProgress: state of exam ongoing/ended

    getCurrentLangQuestion()
        returns current question in current language

    getTotalQuestionCount()
        returns sum of total questions in each module 

methods:
    getExamPaperData()
        purpose: get module question data -> questionData, questionList, moduleData
        parameters: none
        methods: getQuestion()
    
    getQuestion()
        purpose: loads current question with its state(status, time)
        parameters: index of question
        methods: tickTock()
    
    tickTock()
        purpose: timer to measure the time spent on question
        parameters: mins, secs
    
    submitOption()
        purpose: stores user response, updates question status to 'answered' and swtich to next question
        parameters: none
        methods: goToNext(), decreaseCount()
    
    markOption()
        purpose: marks the question for review and updates question status to 'answerMarked' if an option is selected else 'marked'. switch to next question
        parameters: none
        methods: goToNext(), decreaseCount()

    clearResponse()
        purpose: clear the selected response for an question and updates the question status to 'visited'
        parameters: none
        methods: decreaseCount()

    goToNext()
        purpose: check if next question is last in module switch module and if its the last module then submit else just get question
        parameters: none
        methods: getQuestion()

    decreaseCount()
        purpose: decrease the count of question status 
        parameters: status array
    
    disableLangSelect()
        purpose: check if number of languages available is one or more
        parameters: none
        returns false if more than language is available else true

    submitExam()
        purpose: stores the user response from local variable to sessionStorage
        methods: **stopInputStream(), **save()
        parameters: none

    saveState()
        purpose: stores the local variables in sessionStorage on reload
        parameters: none
        methods: **save()

/***********************************************************************************************************/

### Component --> QuestionNumberPanel.vue

props:
    count:
        type: object 
        count of questions which are answered, marked, answerMarked, visited
    total:
        type: Number
        sum of total number of questions in each module
    questionList:
        type: Array
        list of questions of current module
    getQuestion:
        type: Function
        method of ExamPaper.view passed as prop

components: Legend.vue

computed:
    getCandidateName()
        purpose: to get the name of the getCandidateName
        parameters: none
        returns candidate name


/***********************************************************************************************************/


### Component --> Legend.vue

props:
    count
        type: object 
        count of questions which are answered, marked, answerMarked, visited
    total
        type: Number
        sum of total number of questions in each module

/***********************************************************************************************************/

### Component --> QuestionTab.vue

props:
    currentQuestionData
        type:object
        current question object
    value
        type: string
        quetions response value
methods:
    getQuestionData()
        purpose: get required data for a question
        parameters: prop, type: String, values - 'instructions', 'options', 'statement'
        return value of property passed as parameter
    
    updateResponse()
        purpose: emits input event on candidate's response
        parameter: input event

/***********************************************************************************************************/

### View --> ExamFeedback.vue

>>NOTE: **function() are present in mixins

props: none

mixins: ajaxCallMixin, localDb

components: none

lifecycle hooks: none

navigation gaurds:
    beforeRouteEnter()
        push to 'exam/paper' if exam has not been submitted

    beforeRouteLeave()
        block navigation to 'exam/paper', to prevent back button

methods:
    handleSubmit()
        purpose: submit the exam and send feedback response to server
        parameters: none
        methods: **get(), **ajaxCall()
        API call: yes
            url: '/test/test/saveTestObject'
            callback: callbackSubmit()
    
    callbackSubmit()
        purpose: if exam is submitted successfully clear sessionStorage and push to 'exam/end'
        parameters: apiResponse

/***********************************************************************************************************/


### View --> ExamEnd.vue

>>NOTE: **function() are present in mixins

props: none

mixins: localDb

components: none

lifecycle hooks: none

navigation gaurds:
    beforeRouteLeave()
        block navigation to 'exam/feedback', to prevent back button only allow navigation to '/auth/login'

methods:
    logout()
        purpose: clears localStorage and redirect to login
        methods: **remove()

/***********************************************************************************************************/





    
    

    




