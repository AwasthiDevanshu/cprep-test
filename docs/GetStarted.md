# Cprep-Test

White-labelled web and app assessment (test portal) solution for various recruitment and entrance exams

## Getting started

Follow these instructions to run the project

## Set up vue-cli

npm install -g @vue/cli

vue --verison

## Next Steps

- Clone this repository.
- `cd` into `cprep-test`.
- `npm install` to get all the dependencies.
- `npm run serve` to start development server @ http://localhost:8080

